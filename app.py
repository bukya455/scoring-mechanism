# # Import modules
import re
import string
import os
import pandas as pd
from flask import Flask, Response, request, jsonify
import json
import torch
from tqdm import tqdm
import time
# from helper functions
from scoring_utils.score_process_utils import information_extraction2
from scoring_utils.score_process_utils import all_calculations, count_labels_between_columns
from scoring_utils.score_process_utils import calculate_actual_score, calculate_final_score

app = Flask(__name__)

# Home route
@app.route('/')
def index():
    return jsonify({'message': 'Nothing is here.'})

# API Endpoint for NER Entity Extraction
@app.route('/scoring-mechanism', methods=['GET', 'POST'])
def category_entity_extraction():
    if request.method == 'POST':
        data = request.json
        account_numbers_list = data.get("account_numbers_list", [])
        response_list = []  

        start_time = time.time()
        
        # load dataset with NER details
        df = pd.read_csv('./scoring_utils/transaction_data_v0.csv')
        
        for acc_num in account_numbers_list:            
            try:
                # information_extraction2 function takes dataframe, list of account numbers and
                # which year scoring requires -  default used '2023'
                merged_df = information_extraction2(df, [acc_num], ['2023'])

                #merged_df = merged_df.astype(str)
            
                cal_df = all_calculations(merged_df)
                # Apply the function row-wise to the DataFrame
                cal_df['grades_sum'] = cal_df.apply(lambda row: count_labels_between_columns(row,
                                                                                             'total_expenses_grades',
                                                                                             'total_income_grades'), axis=1)

                # Apply the function to each row of the DataFrame containing label counts
                cal_df['user_score'] = cal_df['grades_sum'].apply(calculate_actual_score)

                final_df = calculate_final_score(cal_df)
                final_df.drop(columns=['total_expenses_grades', 'total_income_grades', 'grades_sum'], inplace=True)  # Specify column names to be deleted

            
                final_df = final_df.astype(str)
            
                # Prepare the response with the merged dataframe
                response = {
                    'Input Account Number': acc_num,
                    'Monthly-wise Results': final_df.to_dict(orient='records')
                    }
                
                # output
                response_list.append(response)
                
            except Exception as ex:
                print(ex)
                message = {'Input Account Number': acc_num, 'Error Message': 'Something went wrong with Account Number!'}
                response_list.append(message)
                
        # Inference time 
        print("#messages: ", len(account_numbers_list), "inference Time: ", time.time() - start_time)
            
        return jsonify(response_list)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

