# import libraries
import re
import datefinder
import pandas as pd
import numpy as np


# helper functions
# convert dates in 2023-02-24 format
def date_finder(text):
    temp = []
    text = str(text).lower()
    matches = datefinder.find_dates(text)
    for temp_date in matches:
        temp.append(str(temp_date).split()[0])
    if temp:
        return temp[0]
    else:
        return None

def credited_amount(df, account_num):
    if str(df['credited_acc'])==str(account_num):
        credited_amt = df['transaction_amount']
    else:
        credited_amt = None
    return credited_amt

def debited_amount(df,account_num):
    if str(df['debited_acc'])==str(account_num):
        debited_amt = df['transaction_amount']
    else:
        debited_amt = None
    return debited_amt

# expenses in first 10days, mid 10days, last 10days
def expenses_in_different_time_periods(df2):
    result = []
    for period_month in df2['date_format'].dt.to_period("M").unique():
        month_df = df2[df2['date_format'].dt.to_period("M") == period_month]
        #print(period_month)

        first_10_days_percentage = 0
        mid_10_days_percentage = 0
        last_10_days_percentage = 0

        start_date = month_df['date_format'].min()
        end_date = month_df['date_format'].max()
        time_period = (end_date - start_date) // 3

        first_10_days = start_date + time_period
        mid_10_days = first_10_days + time_period
        last_10_days = mid_10_days + time_period

        first_10_days_expenses = month_df[(month_df['date_format'] >= start_date) & (month_df['date_format'] < first_10_days)]
        mid_10_days_expenses = month_df[(month_df['date_format'] >= first_10_days) & (month_df['date_format'] < mid_10_days)]
        last_10_days_expenses = month_df[(month_df['date_format'] >= mid_10_days) & (month_df['date_format'] <= end_date)]

        total_expenses = month_df['debited_amount'].sum()
        if total_expenses>0:
            first_10_days_percentage = first_10_days_expenses['debited_amount'].sum() / total_expenses * 100
            mid_10_days_percentage = mid_10_days_expenses['debited_amount'].sum() / total_expenses * 100
            last_10_days_percentage = last_10_days_expenses['debited_amount'].sum() / total_expenses * 100
       
        # 'month_year': period_month,
        result.append({
            'month_year': period_month,
            'First_10_Days_Percentage': first_10_days_percentage,
            'Mid_10_Days_Percentage': mid_10_days_percentage,
            'Last_10_Days_Percentage': last_10_days_percentage
        })
        
    result_df = pd.DataFrame(result)
    return result_df

def income_sources(filtered_df):
    # Group by month and income source, and calculate the sum of credited amounts
    monthly_summary = filtered_df.groupby(['Year', df['month_year'], 'mode_of_transaction']).agg({'credited_amount': 'sum'}).reset_index()

    # temp dataframe 
    temp_df = pd.pivot_table(monthly_summary, values='credited_amount', index=['month_year'], columns='mode_of_transaction', fill_value=0).reset_index()

    # total income for each month
    temp_df['total_income'] = temp_df[['upi', 'wallet']].sum(axis=1)

    # percentage of income from each source
    for source in ['upi', 'wallet']:
        if temp_df['total_income']>0:
            temp_df[f'{source}_percentage'] = (temp_df[source] / temp_df['total_income']) * 100
            
    return temp_df

def peak_expense_time_periods(df):
    # monthly expenses
    monthly_expenses = df.groupby(df['date_format'].dt.to_period("M")).sum()
    
    # highest expense over different time periods (3 months, 6 months, 12 months)
    peak_expense_3_months = monthly_expenses['debited_amount'].rolling(window=3, min_periods=1).sum().max()
    peak_expense_6_months = monthly_expenses['debited_amount'].rolling(window=6, min_periods=1).sum().max()
    peak_expense_12_months = monthly_expenses['debited_amount'].rolling(window=12, min_periods=1).sum().max()
    result= {'peak_expense_3_months': peak_expense_3_months,
            'peak_expense_6_months': peak_expense_6_months,
            'peak_expense_12_months': peak_expense_12_months}
    return result

def peak_income_time_periods(df):
    # monthly expenses
    monthly_income = df.groupby(df['date_format'].dt.to_period("M")).sum()
    
    # highest expense over different time periods (3 months, 6 months, 12 months)
    peak_income_3_months = monthly_income['credited_amount'].rolling(window=3, min_periods=1).sum().max()
    peak_income_6_months = monthly_income['credited_amount'].rolling(window=6, min_periods=1).sum().max()
    peak_income_12_months = monthly_income['credited_amount'].rolling(window=12, min_periods=1).sum().max()
    result= {'peak_income_3_months': peak_income_3_months,
             'peak_income_6_months': peak_income_6_months,
            'peak_income_12_months': peak_income_12_months}
    return result

def information_extraction(df2, account_num, year):
    # convert dates in 2023-02-24 format
    df2['date_format'] = df2['date'].apply(date_finder)
    df2['date_format'] = pd.to_datetime(df2['date_format'], errors='coerce')
    # extract month and year
    df2['month_year'] = df2['date_format'].dt.to_period('M')
        
    # extract year from month_year column
    df2['Year'] = df2['month_year'].dt.year

    # transaction amount to debited and credited amount
    df2['credited_amount'] = df2.apply(credited_amount,args = (account_num,),axis=1)
    df2['debited_amount'] = df2.apply(debited_amount,args = (account_num,), axis=1)

    print(df2.head())
    # data for which year to calculate
    desired_years = [int(year)]
    filtered_df = df2[df2['Year'].isin(desired_years)]
    
    # ................. monthly income .................. #
    monthly_income_df = filtered_df.groupby([df2['month_year']]).agg(
        total_income=pd.NamedAgg(column='credited_amount', aggfunc='sum')).reset_index()
    
    # ................. monthly expenses .................. #
    monthly_expenses_df = filtered_df.groupby([df2['month_year']]).agg(
        total_expenses=pd.NamedAgg(column='debited_amount', aggfunc='sum')).reset_index()

    # ......... expenses in first 10days, mid 10days, last 10days ........ #
    expenses_time_periods_df = expenses_in_different_time_periods(filtered_df)
    
    # ................. income sources .................. # 
    income_source_df = income_sources(filtered_df)
    
    # Merge DataFrames side by side based on the common column 'Month'
    merged_df = pd.concat([monthly_income_df.set_index('month_year'),
                           monthly_expenses_df.set_index('month_year'),
                           expenses_time_periods_df.set_index('month_year'),
                          income_source_df.set_index('month_year')], axis=1)
    # account number 
    merged_df['account_number'] = [account_num] * len(merged_df)
    
    category_monthly_expenses_df = filtered_df.groupby(['month_year', 'category']).agg(
        total_expenses=pd.NamedAgg(column='debited_amount', aggfunc='sum')).reset_index()
    
    peak_expenses  = peak_expense_time_periods(filtered_df)
    peak_income = peak_income_time_periods(filtered_df)
    return merged_df, peak_expenses, peak_income, category_monthly_expenses_df

# currently using
def information_extraction2(df2, account_nums, years):
    # Initialize an empty list to store individual DataFrames for each account_num
    merged_dfs = []
    peak_expenses_list = []
    peak_income_list = []
    category_monthly_expenses_list = []

    for account_num in account_nums:
        for year in years:
            # Convert dates to the required format
            df2['date_format'] = df2['date'].apply(date_finder)
            df2['date_format'] = pd.to_datetime(df2['date_format'], errors='coerce')
            # Extract month and year
            df2['month_year'] = df2['date_format'].dt.to_period('M')

            # Extract year from month_year column
            df2['Year'] = df2['month_year'].dt.year

            # Transaction amount to debited and credited amount
            df2['credited_amount'] = df2.apply(credited_amount, args=(account_num,), axis=1)
            df2['debited_amount'] = df2.apply(debited_amount, args=(account_num,), axis=1)

            # Filter data for the specified year
            desired_years = [int(year)]
            filtered_df = df2[df2['Year'].isin(desired_years)]

            # Calculate monthly income
            monthly_income_df = filtered_df.groupby([df2['month_year']]).agg(
                total_income=pd.NamedAgg(column='credited_amount', aggfunc='sum')).reset_index()

            # Calculate monthly expenses
            monthly_expenses_df = filtered_df.groupby([df2['month_year']]).agg(
                total_expenses=pd.NamedAgg(column='debited_amount', aggfunc='sum')).reset_index()

            # Calculate expenses in different time periods
            expenses_time_periods_df = expenses_in_different_time_periods(filtered_df)

            # Calculate income sources
            #income_source_df = income_sources(filtered_df)

            # Merge DataFrames based on the common column 'Month'
            merged_df = pd.concat([monthly_income_df.set_index('month_year'),
                                   monthly_expenses_df.set_index('month_year'),
                                   expenses_time_periods_df.set_index('month_year')], 
                                  axis=1)
                                   #income_source_df.set_index('month_year')], axis=1)
            # Set account number
            merged_df['account_number'] = [account_num] * len(merged_df)

            # Group by month_year and sum the duplicate columns
            merged_df = merged_df.groupby(level=0, axis=1).sum()

            # Append the results to the respective lists
            merged_dfs.append(merged_df.reset_index())
            # The rest of the code remains the same...

    # Concatenate the DataFrames in the lists
    merged_df = pd.concat(merged_dfs)
    # The rest of the code remains the same...
    #print(merged_df.shape)
    return merged_df


# use as
# merged_df = information_extraction2(df, acc_list, ['2023'])



# # apply the grading for total_bins
# helper function
def calculate_total_grades(df):
    # list of columns
    # columns = ['total_expenses', 'total_income', 'total_available_credit']
    columns = ['total_expenses', 'total_income']
    # bin and labels
    bins_labels = {
        'total_bins': [0, 20000, 40000, 60000, 80000, 100000000000],
        'total_labels':['E', 'D', 'C', 'B', 'A']
    }
    
    for col in columns:
        # Apply the grading for each specified column
        df[f'{col}_grades'] = pd.cut(df[col], 
                                      bins=bins_labels['total_bins'], 
                                      labels=bins_labels['total_labels'], right=False)
    
    return df

def all_calculations(df):
    # Apply the calculation for total grades
    df_total = calculate_total_grades(df)
    
    # Apply the calculation for percentage grades
    #df_percent = calculate_percentage_grades(df)
    
    # Apply the payments mapping
    #df_payment = payments_mapping(df)
    
    # Combine the DataFrames
    #combined_df = pd.concat([df_total, df_percent, df_payment], axis=1)
    
    return df_total # df_payment


# Define a function to count occurrences of labels 'F' to 'J' in each row between two columns
def count_labels_between_columns(row, start_col, end_col):
    counts = row[start_col:end_col].value_counts()
    label_counts = [f"{counts.get(label, '')}{label}" for label in ['A','B','C','D','E','F', 'G', 'H', 'I', 'J'] if counts.get(label, 0) > 0]
    return '+'.join(label_counts)
# calculate the final score based on label counts and their values
def calculate_actual_score(label_counts):
    label_values = {'A': 100, 'B': 90, 'C': 80, 'D': 70, 'E': 60, 'F': 10, 'G': -10, 'H': -20, 'I': -30, 'J': -40}
    score = 0
    for item in label_counts.split('+'):
        count, label = item[:-1], item[-1]
        if count:
            score += int(count) * label_values[label]
    return score

# final_score
def calculate_final_score(df):
    # bin and labels
    bins_labels = {'final_bins':[0, 550, 600, 650, 700, 900],
                   'final_labels':[1, 2, 3, 4, 5] }
    

    # apply the grading for total_bins
    df['final_score'] = pd.cut(df['user_score'], 
                               bins=bins_labels['final_bins'], 
                               labels=bins_labels['final_labels'], right=False)
    
    return df
